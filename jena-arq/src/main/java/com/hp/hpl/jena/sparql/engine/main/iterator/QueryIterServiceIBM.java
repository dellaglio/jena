package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;
import org.apache.jena.atlas.logging.Log ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.QueryExecException ;
import com.hp.hpl.jena.sparql.algebra.Op ;
import com.hp.hpl.jena.sparql.algebra.op.OpService ;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding ;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterSingleton ;
import com.hp.hpl.jena.sparql.engine.main.QC ;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

	public class QueryIterServiceIBM extends QueryIterRepeatApply
	{
		public static int callCount=0;
		OpService opService ;
		//Set<Var> keyVars;
		Cache<Binding,Set<Binding>> cache;
		private HashMap<Binding, Long> cacheBBT;
		private HashMap<Binding,Integer> changeRate;
		long tnow;
		Set<Binding> outerContent = null;
		HashMap<Binding,Double> outerContentL=null;
		QueryIterator outerContentIterator = null;

		Set<Binding> electedList = new HashSet<Binding>();
		public static Logger logger = LoggerFactory.getLogger(QueryIterServiceIBM.class);
		
		public QueryIterServiceIBM(QueryIterator input, OpService opService, ExecutionContext context)
		{
			super(input, context) ;
			if ( context.getContext().isFalse(Service.serviceAllowed) )
				throw new QueryExecException("SERVICE not allowed") ; 
			this.opService = opService ;
			
			int slide=0,width=0;
			//fill the window content
			//computing L value
			cache = CacheAcqua.INSTANCE.getCache();
			
			if(outerContent == null){
				outerContent = new HashSet<Binding>();
				outerContentL=new HashMap<Binding,Double>();

				Context ec= context.getContext();
				tnow=Integer.parseInt(ec.getAsString(Symbol.create("acqua:tnow")));	
				width=Integer.parseInt(ec.getAsString(Symbol.create("acqua:width")));
				slide= Integer.parseInt(ec.getAsString(Symbol.create("acqua:slide")));
				
				while(input.hasNext()){
					Binding b = input.next();
					
					BindingBase b1 = (BindingBase) b;
					Long t = null;
					while(b1 instanceof TimestampedBindingHashMap || b1.getParent()!=null){
						if(b1 instanceof TimestampedBindingHashMap){
							t = ((TimestampedBindingHashMap)b1).getMaxTimestamp();
							System.out.println("- min ts ->"+((TimestampedBindingHashMap)b1).getMinTimestamp());
							System.out.println("- max ts ->"+((TimestampedBindingHashMap)b1).getMaxTimestamp());
							break;
						}
						b1=(BindingBase) b1.getParent();
					}
					double L=Math.abs(Math.ceil(t+width-tnow / slide));//L is always negative because tnow is always larger than entrance timestamp;
					//logger.debug("L value of the mapping{}: ceil({}+ {} -{} / {} )={}",b, t ,width, tnow,slide,L);
					BindingHashMap keyWindowBinding=new BindingHashMap();
					Iterator<Var> keyVars = CacheAcqua.INSTANCE.getKeyVars().iterator();
					while(keyVars.hasNext()){
						Var tempKeyVar=keyVars.next();
						if(b.get(tempKeyVar)!=null)
							{
								keyWindowBinding.add(tempKeyVar,b.get(tempKeyVar));
							}
					}
					outerContent.add(b);
					//logger.debug("adding L to outerContentL {}",outerContentL);
					Double prevL = outerContentL.get(keyWindowBinding);
					if(prevL==null)
						outerContentL.put(keyWindowBinding,L);
					else
						outerContentL.put(keyWindowBinding,Math.min(L,prevL));
				}
				outerContentIterator =  new QueryIterPlainWrapper(outerContent.iterator());
			}
			
			
			//filling bbt and changeRate
			//elected set formation requires access to cache elements and their bbt and window setting
			cacheBBT=CacheAcqua.INSTANCE.getCacheBBT();
		    changeRate = CacheAcqua.INSTANCE.getCacheChangeRate();	
			ArrayList<keySet> scoreHM=new ArrayList<keySet>(); 
			ArrayList<keySet> notExpired=new ArrayList<keySet>(); 
			Iterator<Binding> windowKeyIt = outerContentL.keySet().iterator();
			while(windowKeyIt.hasNext()){
				Binding key = windowKeyIt.next();
				logger.debug("key {} ",key);
				long bbt=cacheBBT.get(key);
				long cr=changeRate.get(key);
				double times = Math.ceil((double)(tnow-bbt) / (double)cr);
				double v = (bbt+cr*times-tnow)/slide;
				//logger.debug("V value of the mapping{}: (bbt {} + cr {} * times {} -tnow {}) / slide {} )= {}",key, bbt ,cr, times,tnow,slide,v);					
				double l = outerContentL.get(key);
				if(bbt<tnow){//if cached entry is already expired we compute the V value
				scoreHM.add(new keySet(key, l, v));
				}else {
					notExpired.add(new keySet(key, l, v));
				}
			}
			Collections.sort(scoreHM, new ValueComparator());
			int updateBudget = width/4;//outerContent.size();//slide;//0;//costPerAccess; 
	        Iterator< keySet> sit= scoreHM.iterator();
	        Iterator< keySet> fit= notExpired.iterator();
	        for(int y=0;y<updateBudget ;y++){
	        	keySet tempB;
	        	if(sit.hasNext()){
	        		tempB=sit.next();
	        	}else{
	        		tempB=fit.next();
	        	}
	        	electedList.add(tempB.keys);
	        	//logger.debug(">>>>>>>>>>>>>>>>> elected {} ",tempB.keys);
	        }
	        //System.out.println("for the full wbm all arrays should have same size: "+scoreHM.size()+","+electedList.size()+","+outerContent.size()+","+outerContentL.size());        
		}
		
		protected QueryIterator getInput() {
			return outerContentIterator;
		};

		
		@Override
		protected QueryIterator nextStage(Binding outerBinding)
		{
			
			//outerbinding contains the values for the window variables
			Binding key = extractKey(outerBinding);
			
			//System.out.println("required key : "+key+" cache content is ");
			//Iterator<Binding> cit = cache.keys();
			//while (cit.hasNext())
			//System.out.println(cit.next());
			//we should always retrive from cache but rather to check if we have update budget to fetch the data from service
			//we assume cache contains all the keys ==> materialize at the query registeration time
			//we fetch the response for a subset of bindings ...
			
			if(!electedList.contains(key)){
				//logger.debug(".........................not elected {} retrived from cache ",key);
				Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
				
				QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
				
				//((BindingHashMap)outerBinding)
				QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
				return qIter2 ;
			}
			else{
				//logger.debug("...........................elected {} fetched and replaced in cache ",key);
				callCount ++;
				Op op = QC.substitute(opService, outerBinding) ;
				//logger.debug(">>>>>>>>>>>>>>>>>substituted op {}, original op {}, bindining {}",op,opService,outerBinding);
				boolean silent = opService.getSilent() ;
				try {
					QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
					// This iterator is materialized already otherwise we may end up
					// not servicing the HTTP connection as needed.
					// In extremis, can cause a deadlock when SERVICE loops back to this server.
					// Add tracking.
					qIter = QueryIter.makeTracked(qIter, getExecContext()) ;

					Set<Binding> valuesOfKey= new HashSet<Binding>();
					while(qIter.hasNext()){
						valuesOfKey.add(qIter.next()); 
					}
					
					cache.put(key, valuesOfKey);
					long bbt=cacheBBT.get(key);
					long cr=changeRate.get(key);
					double times = Math.ceil((double)(tnow-bbt) / (double)cr);
					
					cacheBBT.put(key, bbt+cr*(long)times);
					//logger.debug("refreshing {} : BBT moved to {}",key,bbt+cr*times);
					//logger.debug("caching {} >> {}",key,valuesOfKey);
					qIter = new QueryIterPlainWrapper(valuesOfKey.iterator());
					QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;

					return qIter2;
				} catch (RuntimeException ex)
				{
					if ( silent )
					{
						Log.warn(this, "SERVICE <" + opService.getService().toString() + ">: " + ex.getMessage()) ;
						// Return the input
						return QueryIterSingleton.create(outerBinding, getExecContext()) ; 
					}
					throw ex ;
				}

			}
		}

		
		private HashMap<Binding, Set<Binding>> Elect() {
			// TODO Auto-generated method stub
			return null;
		}
	
		
	}
	

