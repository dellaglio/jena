package com.hp.hpl.jena.sparql.engine.acqua;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import jena.rdfcat;

import org.apache.jena.atlas.lib.Cache;
import org.apache.jena.atlas.lib.cache.CacheLRU;
import org.apache.log4j.lf5.util.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.algebra.OpAsQuery;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.BindingProjectNamed;
import com.hp.hpl.jena.sparql.engine.binding.BindingUtils;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.expr.nodevalue.NodeValueInteger;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

public class CacheAcqua {

	public static Logger logger = LoggerFactory.getLogger(CacheAcqua.class);
	public static final CacheAcqua INSTANCE = new CacheAcqua();
	
	private CacheLRU<Binding, Set<Binding>> cache;
	private HashMap<Binding, Long> cacheBBT;
	public HashMap<Binding, Integer> cacheChangeRate;
	public HashMap<Binding, Long> lastUpdateTimeInCache;//used for LRU
	public HashMap<Binding, Long> lastUpdateTimeInRemote;//used for LRU
	
	public HashMap<Binding, String> followersBank;
	private Set<Var> cacheKeyVars;

	public HashMap<Binding, Integer> bindingToNumID;
	public HashMap<Integer, Binding> numIDToBinding;
	
	private CacheAcqua() {// OpService opService){
		//System.out.println(">>> contructor of cache acqua is called");
		//Thread.dumpStack();
		cache = new CacheLRU<Binding, Set<Binding>>(0.8f, 100);//1000);
		cacheBBT = new HashMap<Binding, Long>();
		lastUpdateTimeInCache=new HashMap<Binding, Long>();
		lastUpdateTimeInRemote=new HashMap<Binding, Long>();
		cacheChangeRate = new HashMap<Binding, Integer>();
		followersBank = new HashMap<Binding, String>();
		bindingToNumID = new HashMap<Binding, Integer>();
		numIDToBinding = new HashMap<Integer, Binding>();
	}

	public Set<Var> getKeyVars() {
		return cacheKeyVars;
	}

	
	
	public void init(OpService opService, Set<Var> keyVars, long curTime) {
		cache.clear();
		cacheBBT.clear();
		lastUpdateTimeInCache.clear();
		lastUpdateTimeInRemote.clear();
		cacheChangeRate.clear();
		followersBank.clear();
		// logger.debug("context dataset {} , context active graph {}, context executor {} ",ec.getDataset(),ec.getActiveGraph(),ec.getExecutor());
		this.cacheKeyVars = keyVars;
		Node endpoint = opService.getService();
		Query query = OpAsQuery.asQuery(opService.getSubOp());
		// logger.debug("endpoint : {} , opService.getSubOp() {}, query {} ",endpoint,opService.getSubOp(),query);

		// Query query = QueryFactory.create(sparqlQuery1);
		QueryExecution qe = QueryExecutionFactory.sparqlService(
				endpoint.getURI(), query);
		ResultSet as = qe.execSelect();
		for (; as.hasNext();) {
			QuerySolution qs = as.nextSolution();

			// logger.debug("query solution {} ",qs);
			BindingProjectNamed solb = (BindingProjectNamed) BindingUtils
					.asBinding(qs);
			// solb.add(Var.alloc("bbt"),
			// NodeFactory.createLiteral(Long.toString(initTime)));
			// solb.add(Var.alloc("changeRate"),
			// NodeFactory.createLiteral(Long.toString(r.nextLong())));
			// BindingUtils.merge
			BindingHashMap tempKeyBinding = new BindingHashMap();
			Iterator<Var> varIt = keyVars.iterator();
			while (varIt.hasNext()) {
				Var keyVar = varIt.next();
				Node keyBinding = solb.get(keyVar);
				tempKeyBinding.add(keyVar, keyBinding);
			}

			BindingHashMap solBindingWithoutKey = new BindingHashMap();
			Iterator<Var> solVars = solb.vars();
			while (solVars.hasNext()) {
				Var nextSolVar = solVars.next();
				if (!keyVars.contains(nextSolVar)) {
					solBindingWithoutKey.add(nextSolVar, solb.get(nextSolVar));
				}
			}

			Set<Binding> currentKeyBindings = cache.get(tempKeyBinding);
			if (currentKeyBindings == null) {
				currentKeyBindings = new HashSet<Binding>();
				// currentKeyBindings.add(solb);
				currentKeyBindings.add(solBindingWithoutKey);
			} else {
				// currentKeyBindings.add(solb);
				currentKeyBindings.add(solBindingWithoutKey);
			}
			cache.put(tempKeyBinding, currentKeyBindings);
			//System.out.println("----->" + tempKeyBinding);
			// logger.debug("caching key {} >> added{} ",tempKeyBinding,
			// solb);//Iterator<String> strs = soln.varNames();
		}
		//System.out.println("-->" + cache.size());

		qe.close();
		Random r = new Random(System.currentTimeMillis());

	Iterator<Binding> keys = cache.keys();
		//int ccc = 0;
		while (keys.hasNext()) {
			//ccc++;
			Binding nextB = keys.next();
			//switch from external time to internal time	
			//Node test = nextB.get(Var.alloc("S"));
			//System.out.println(test.getURI());
			cacheBBT.put(nextB, curTime);//initTime);
			lastUpdateTimeInCache.put(nextB, curTime);
			lastUpdateTimeInRemote.put(nextB, curTime);
			// logger.debug("BBT {} : {} ",nextB,initTime);
		}
		//System.out.println("------>" + ccc);
		
	}
	
	public void readCRandFB(){
		try{
			BufferedReader br=new BufferedReader(new FileReader(
	    			 new File(QueryExecUtils.bkgpath)));
	    	//BufferedReader fr = new BufferedReader(new FileReader(new File("/ichec/home/users/sdehghanzadeh/followers2_100.txt")));
			 //BufferedReader fr = new BufferedReader(new FileReader(new File("../acquaCsparql/followers2_1000.txt")));
			BufferedReader fr = new BufferedReader(new FileReader(new File("/home/soheila/git/newcsparql/acquaCsparql/followers2_100.txt")));
	    	 String line=null;
	    	 while((line=br.readLine())!=null){
	    		 String[] prts = line.split(" ");
	    		 BindingHashMap tmp = new BindingHashMap();
	    		 
	    		 tmp.add(Var.alloc("S"), NodeFactory.createURI(prts[0].substring(1,prts[0].length()-1)));
	    		 cacheChangeRate.put(tmp, Integer.parseInt(prts[1]));
	    		 followersBank.put(tmp,fr.readLine());
	    		 lastUpdateTimeInCache.put(tmp, 0L);
	    		 lastUpdateTimeInRemote.put(tmp, 0L);
	    		 //int numID = Integer.parseInt(prts[0].substring(23,prts[0].length()-1))-1;
	    		 //bindingToNumID.put(tmp, numID);
	    		 //numIDToBinding.put(numID, tmp); 
	    	 }
	    	 //bipartiteMappingGraph = new BiadjacencyMatrix("../acquaCsparql/BKG/graph/L50Skew_R50Rand_1000.txt");
	    	 
	     } catch(Exception e) {
	    	 logger.error("error while reading changeRates!!!", e); 
	     }
	}

	//used to update fuseki server elements based on their change rates
	public void serverUpdater() {
		long start = System.currentTimeMillis() / 1000;
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		while (true) {
			Iterator<Binding> users = cacheChangeRate.keySet().iterator();
			long time = System.currentTimeMillis();
			long t = time / 1000;
			// System.out.println(">>>>>>>>>time of updating server: "+t);
			while (users.hasNext()) {
				Binding curUser = users.next();

				long cr = (long) cacheChangeRate.get(curUser);
				String FB = followersBank.get(curUser);
				// System.out.println(curUser+" cr= "+cr+" t/m "+t);
				if ((t - start) % cr == 0) {
					logger.debug("update request user "+curUser);
					/*
					 * FIXME: i assume there is only 1 var in shared variables.
					 * however they can be many and then i need to find another
					 * way to update the cache
					 */
					String u = curUser.get(curUser.vars().next()).toString();
					String UQ = "DELETE    { <"
							+ u
							+ "> <http://example.org/follower> ?a } where{<"
							+ u
							+ "> <http://example.org/follower> ?a}; INSERT data {<"
							+ u
							+ "> <http://example.org/follower> <http://localhost/"
							+ FB.split(",")[(int) (((t - start) / cr) % 20)]
							+ ">} ";
					// if ((t-start)/cr>20)
					// {System.out.println("&&&&&&&&&&&&&&&&&&a user has passed more than one round in the list of followers");System.exit(0);}
					UpdateRequest query = UpdateFactory.create(UQ);
					UpdateProcessor qexec = UpdateExecutionFactory
							.createRemoteForm(query,
									"http://localhost:3030/test/update");
					qexec.execute();
				}
			}
			long time2 = System.currentTimeMillis();
			if (time2 - time < 1000)
				try {
					Thread.sleep(1000 - (time2 - time));
				} catch (InterruptedException e) {
					System.out
							.println("internal thread for updating server entries can't sleep!!!");
					e.printStackTrace();
				}

		}
	}

	public Cache<Binding, Set<Binding>> getCache() {
		return cache;
	}

	public HashMap<Binding, Long> getCacheBBT() {
		return cacheBBT;
	}
	public HashMap<Binding, Long> getLastUpdateTimeInCache() {
		return lastUpdateTimeInCache;
	}
	public HashMap<Binding, Integer> getCacheChangeRate() {
		return cacheChangeRate;
	}

}
