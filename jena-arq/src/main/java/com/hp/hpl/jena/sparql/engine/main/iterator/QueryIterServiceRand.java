package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;
import com.hp.hpl.jena.sparql.util.Symbol;

public class QueryIterServiceRand extends QueryIterRepeatApply {
	OpService opService;
	Cache<Binding, Set<Binding>> cache;
	public static long totalTimeCons = 0;
	public static long totalTimeNext = 0;
	public static int callCount = 0;

	public QueryIterServiceRand(QueryIterator input, OpService opService,
			ExecutionContext context) {
		super(input, context);
		// System.out.println(">>>>>>>>>stack Trace of QueryIterService");
		// Thread.dumpStack();
		// if(QueryExecUtils.updateBudget>QueryExecUtils.windowLength) return ;
		callCount = 0;
		long start = System.currentTimeMillis();
		totalTimeCons = 0;
		totalTimeNext = 0;
		this.opService = opService;
		cache = CacheAcqua.INSTANCE.getCache();
		totalTimeCons = System.currentTimeMillis() - start;

	}

	@Override
	protected QueryIterator nextStage(Binding outerBinding) {
		long start = System.currentTimeMillis();
		Binding key = extractKey(outerBinding);

		if (!(callCount < QueryExecUtils.updateBudget))
		{
			Set<Binding> tmp = cache.get(key);
			if(tmp!=null){
				QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
				QueryIterator qIter2 = new QueryIterCommonParent(qIter,
					outerBinding, getExecContext());
				totalTimeNext += (System.currentTimeMillis() - start);
				return qIter2;}
			else{
				System.out.println(key+" windows entry without matching entry in cache!!!");
				return null;

			}
			
		} else {
			//boolean silent;
			try {
				callCount++;
				Op op = QC.substitute(opService, outerBinding);
				QueryIterator qIter = Service.exec((OpService) op,
						getExecContext().getContext());
				//silent = opService.getSilent();
				Set<Binding> valuesOfKey = new HashSet<Binding>();
				while (qIter.hasNext()) {
					valuesOfKey.add(qIter.next());
				}
				cache.put(key, valuesOfKey);
				qIter = new QueryIterPlainWrapper(valuesOfKey.iterator());
				QueryIterator qIter2 = new QueryIterCommonParent(qIter,
						outerBinding, getExecContext());
				totalTimeNext += (System.currentTimeMillis() - start);
				return qIter2;
			} catch (Exception e) {
				throw e ;
			}
		}
	}

	

}
