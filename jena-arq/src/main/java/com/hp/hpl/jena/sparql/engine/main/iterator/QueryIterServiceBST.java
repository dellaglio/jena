package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.QueryExecException;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;
import com.hp.hpl.jena.sparql.util.Symbol;

public class QueryIterServiceBST extends QueryIterRepeatApply {
	public static int callCount=0;
	public static long totalTimeNext=0;
	public static long totalTimeCons=0;
	//public static HashMap<Binding,Double> inconsistencyStats=new HashMap<Binding,Double>();
	OpService opService ;
	//Set<Var> keyVars;
	Cache<Binding,Set<Binding>> cache;
	private HashMap<Binding, Long> cacheBBT;
	private HashMap<Binding,Integer> changeRate;
	long tnow;
	Set<Binding> outerContent = null;
	HashMap<Binding,Double> outerContentL=null;
	QueryIterator outerContentIterator = null;

	Set<Binding> electedList = new HashSet();
	public static Logger logger = LoggerFactory.getLogger(QueryIterServiceWBM.class);
	public static ArrayList<Integer> burnRate=new ArrayList<Integer>();
	public QueryIterServiceBST(QueryIterator input, OpService opService, ExecutionContext context)
	{
		super(input, context) ;
		long start=System.currentTimeMillis();

		if ( context.getContext().isFalse(Service.serviceAllowed) )
			throw new QueryExecException("SERVICE not allowed") ; 
		this.opService = opService ;


		cache = CacheAcqua.INSTANCE.getCache();

		outerContent = new HashSet<Binding>();

		Context ec= context.getContext();
		tnow=Long.parseLong(ec.getAsString(Symbol.create("acqua:tnow")));	
		cacheBBT=CacheAcqua.INSTANCE.getCacheBBT();
		changeRate = CacheAcqua.INSTANCE.getCacheChangeRate();	
		ArrayList<Binding> Expired=new ArrayList<Binding>(); 
		ArrayList<Binding> notExpired=new ArrayList<Binding>(); 
		while(input.hasNext()){
			Binding b = input.next();	
			BindingHashMap keyWindowBinding=new BindingHashMap();
			Iterator<Var> keyVars = CacheAcqua.INSTANCE.getKeyVars().iterator();
			while(keyVars.hasNext()){
				Var tempKeyVar=keyVars.next();
				if(b.get(tempKeyVar)!=null)
					{
						keyWindowBinding.add(tempKeyVar,b.get(tempKeyVar));
					}
			}
			long bbt=cacheBBT.get(keyWindowBinding);
			if(bbt <= tnow){//if cached entry is already expired we compute the V value
				//System.out.println("expired instered");
				Expired.add(keyWindowBinding);				
			}else {	
				//System.out.println("NOT expired instered");
				notExpired.add(keyWindowBinding);
			}
			outerContent.add(b);				
		}
		outerContentIterator =  new QueryIterPlainWrapper(outerContent.iterator());


		Iterator< Binding> sit= Expired.iterator();
		Iterator< Binding> fit= notExpired.iterator();
		for(int y=0;y<QueryExecUtils.updateBudget;y++){
			Binding tempB;
			if(sit.hasNext()){
				tempB=sit.next();
			}else if(fit.hasNext()){
				tempB=fit.next();
			}else { break;}
			electedList.add(tempB);
		}
		totalTimeCons=System.currentTimeMillis()-start;
		        
	}
	protected QueryIterator getInput() {
		return outerContentIterator;
	};
	@Override
	protected QueryIterator nextStage(Binding outerBinding)
	{
		long start=System.currentTimeMillis();
		Binding key = extractKey(outerBinding);
		if(!electedList.contains(key)){
			Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
			if(tmp!=null){
			QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
			QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
			totalTimeNext+=(System.currentTimeMillis()-start);
			return qIter2 ;}
			else{
				System.out.println(key+" windows entry without matching entry in cache!!!");
				return null;

			}
		}
		else{
			System.out.println("retriving  "+outerBinding+" from Remote");
			try {
				callCount ++;
				Op op = QC.substitute(opService, outerBinding) ;
				//silent = opService.getSilent() ;			
				QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
				Set<Binding> valuesOfKey= new HashSet<Binding>();
				while(qIter.hasNext()){
					valuesOfKey.add(qIter.next()); 
				}
				cache.put(key, valuesOfKey);
				//-----------------------
				long bbt=cacheBBT.get(key);
				long cr=changeRate.get(key);
				cacheBBT.put(key, ((tnow / cr)+1) * cr);
				qIter = new QueryIterPlainWrapper(valuesOfKey.iterator());
				QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
				totalTimeNext+=(System.currentTimeMillis()-start);
				return qIter2;
			} catch (Exception ex)
			{
				/*if ( silent )
				{
					Log.warn(this, "SERVICE <" + opService.getService().toString() + ">: " + ex.getMessage()) ;
					// Return the input
					return QueryIterSingleton.create(outerBinding, getExecContext()) ; 
				}*/
				throw ex ;
			}

		}
	}

	
}
