/*
 * (c) Copyright 2007, 2008, 2009 Hewlett-Packard Development Company, LP
 * (c) Copyright 2010 Talis Systems Ltd
 * All rights reserved.
 * [See end of file]
 */

package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.Set;

import org.apache.jena.atlas.lib.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.sparql.algebra.Op ;
import com.hp.hpl.jena.sparql.algebra.op.OpService ;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding ;
import com.hp.hpl.jena.sparql.engine.http.Service ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply ;
import com.hp.hpl.jena.sparql.engine.main.QC ;


public class QueryIterService extends QueryIterRepeatApply
{
    OpService opService ;
    public static Logger logger = LoggerFactory.getLogger(QueryIterService.class);
	public static int callCount=0;
	public static long totalTimeNext=0;
	public static long totalTimeCons=0;
	
    public QueryIterService(QueryIterator input, OpService opService, ExecutionContext context)
    {
        super(input, context) ;
       ///System.out.println(">>>>>>>>>stack Trace of QueryIterService");
		//Thread.dumpStack();		
        callCount=0;
        totalTimeNext=0;
		totalTimeCons=0;
		long start=System.currentTimeMillis();
		this.opService = opService ;        
		totalTimeCons=System.currentTimeMillis()-start;
        
    }
    
    @Override
    protected QueryIterator nextStage(Binding outerBinding)
    {
    	long start=System.currentTimeMillis();
		
    	//System.out.println(">>>>>>>>>stack Trace of nextStagecsparql");
		//Thread.dumpStack();
		
        Op op = QC.substitute(opService, outerBinding) ;
        QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
        callCount++;
        // Need to put the outerBinding as parent to every binding of the service call.
        // There should be no variables in common because of the OpSubstitute.substitute 
        QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;

        // Materialise, otherwise we may have outstanding incoming data.
        // Alows the server to fulfil the request as soon as possible.
        // In extremis, can cause a deadlock when SERVICE loops back to this server.
        totalTimeNext+=(System.currentTimeMillis()-start);
		
        return QueryIter.materialize(qIter2, getExecContext()) ;
    }
}
 

/*
 * (c) Copyright 2007, 2008, 2009 Hewlett-Packard Development Company, LP
 * (c) Copyright 2010 Talis Systems Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */