package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.Set;

import org.apache.jena.atlas.lib.Cache;

import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply;
import com.hp.hpl.jena.sparql.engine.main.QC;

public class QueryIterServiceWST  extends QueryIterRepeatApply{
	public static int callCount=0;
	Cache<Binding,Set<Binding>> cache;
	
	public QueryIterServiceWST(QueryIterator input, ExecutionContext context)
    {
        super(input, context) ;	
        callCount=0;
        cache = CacheAcqua.INSTANCE.getCache();
    }
    
    @Override
    protected QueryIterator nextStage(Binding outerBinding)
    {
    	long start=System.currentTimeMillis();
    	Binding key = extractKey(outerBinding);		
    	Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
		if(tmp!=null){
		QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
		QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
		return qIter2 ;}
		else{
			System.out.println(key+" windows entry without matching entry in cache!!!");
			return null;

		}
    }
}
