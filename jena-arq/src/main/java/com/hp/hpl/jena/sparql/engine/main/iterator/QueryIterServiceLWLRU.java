package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.QueryExecException;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;
import com.hp.hpl.jena.sparql.util.Symbol;

public class QueryIterServiceLWLRU extends QueryIterRepeatApply{
	final class BindingEntry{
		Binding userId;
		long updateTimeDiff;
		public BindingEntry(Binding id,long t){userId=id;updateTimeDiff=t;}	
		public Binding getBinding(){return userId;}
	}
	OpService opService;
	Cache<Binding, Set<Binding>> cache;
	HashMap<Binding, Long> lastUpdateTimeInCache;
	public static long totalTimeCons = 0;
	public static long totalTimeNext = 0;
	public static int callCount = 0;
	Set<Binding> LRUList = new HashSet();
	QueryIterator outerContentIterator = null;
	public QueryIterServiceLWLRU(QueryIterator input, OpService opService,
			ExecutionContext context) {
		super(input, context);
		// System.out.println(">>>>>>>>>stack Trace of QueryIterService");
		// Thread.dumpStack();
		// if(QueryExecUtils.updateBudget>QueryExecUtils.windowLength) return ;
		
		Set<Binding> outerContent = null;
		callCount = 0;
		long start = System.currentTimeMillis();
		totalTimeCons = 0;
		totalTimeNext = 0;
		this.opService = opService;
		cache = CacheAcqua.INSTANCE.getCache();
		totalTimeCons = System.currentTimeMillis() - start;
		lastUpdateTimeInCache=CacheAcqua.INSTANCE.getLastUpdateTimeInCache();	    
		outerContent = new HashSet<Binding>();//to keep the content of window
		Context ec= context.getContext();
		long tnow=Long.parseLong(ec.getAsString(Symbol.create("acqua:tnow")));	
		List<BindingEntry> bindingUpdateLatency=new ArrayList<BindingEntry>();
		while(input.hasNext()){
				Binding b = input.next();				
				outerContent.add(b);
				BindingHashMap keyWindowBinding=new BindingHashMap();
				Iterator<Var> keyVars = CacheAcqua.INSTANCE.getKeyVars().iterator();
				while(keyVars.hasNext()){
					Var tempKeyVar=keyVars.next();
					if(b.get(tempKeyVar)!=null)
						{
							keyWindowBinding.add(tempKeyVar,b.get(tempKeyVar));
						}
				}
				bindingUpdateLatency.add(new BindingEntry(keyWindowBinding,tnow-lastUpdateTimeInCache.get(keyWindowBinding)));
		}
		Collections.sort(bindingUpdateLatency, new Comparator<BindingEntry>() {
			public int compare(BindingEntry o1, BindingEntry o2) {
				int res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				//if(res==0)
				//	res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				return res;
			}
		});
		int cnt=0;
		for (BindingEntry be :bindingUpdateLatency){
			if(cnt<QueryExecUtils.updateBudget){
				LRUList.add(be.getBinding());
				System.out.println("LRU list content: "+be.getBinding());
				cnt++;
			}
			else break;
		}
		outerContentIterator =  new QueryIterPlainWrapper(outerContent.iterator());
		
	}

	protected QueryIterator getInput() {
		return outerContentIterator;
	};
	@Override
	protected QueryIterator nextStage(Binding outerBinding) {
		long start=System.currentTimeMillis();
		Binding key = extractKey(outerBinding);
    	
		if (LRUList.contains(key)){
			System.out.println("key passed to next: "+key);
				callCount ++;
			Op op = QC.substitute(opService, outerBinding) ;
			//silent = opService.getSilent() ;			
			QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
			Set<Binding> valuesOfKey= new HashSet<Binding>();
			while(qIter.hasNext()){
				valuesOfKey.add(qIter.next()); 
			}
			//System.out.println("updating "+tmp + " to "+ valuesOfKey);
			cache.put(key, valuesOfKey);	
			lastUpdateTimeInCache.put(key, start);
    		} 	
    	
    	Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
    	//System.out.println("retriving "+key + " with value "+ tmp);
		if(tmp!=null){
		QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
		
		//((BindingHashMap)outerBinding)
		QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
		totalTimeNext+=(System.currentTimeMillis()-start);
		return qIter2 ;}
		else{
			System.out.println(key+" windows entry without matching entry in cache!!!");
			return null;

		}    	
	}

	

}
