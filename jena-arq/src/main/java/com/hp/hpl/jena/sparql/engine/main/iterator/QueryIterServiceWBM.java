/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;
import org.apache.jena.atlas.logging.Log ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.QueryExecException ;
import com.hp.hpl.jena.sparql.algebra.Op ;
import com.hp.hpl.jena.sparql.algebra.op.OpService ;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding ;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterSingleton ;
import com.hp.hpl.jena.sparql.engine.main.QC ;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;
import com.hp.hpl.jena.sparql.util.Symbol;

public class QueryIterServiceWBM extends QueryIterRepeatApply
{
	public static int callCount=0;
	public static long totalTimeNext=0;
	public static long totalTimeCons=0;
	//public static HashMap<Binding,Double> inconsistencyStats=new HashMap<Binding,Double>();
	OpService opService ;
	//Set<Var> keyVars;
	Cache<Binding,Set<Binding>> cache;
	private HashMap<Binding, Long> cacheBBT;
	private HashMap<Binding,Integer> changeRate;
	long tnow;
	Set<Binding> outerContent = null;
	HashMap<Binding,Double> outerContentL=null;
	QueryIterator outerContentIterator = null;

	Set<Binding> electedList = new HashSet();
	public static Logger logger = LoggerFactory.getLogger(QueryIterServiceWBM.class);
	public static ArrayList<Integer> burnRate=new ArrayList<Integer>();
	
	public QueryIterServiceWBM(QueryIterator input, OpService opService, ExecutionContext context)
	{
		super(input, context) ;
		//Thread.dumpStack();
		callCount=0;
		totalTimeNext=0;
		totalTimeCons=0;
		long start=System.currentTimeMillis();
		
		if ( context.getContext().isFalse(Service.serviceAllowed) )
			throw new QueryExecException("SERVICE not allowed") ; 
		this.opService = opService ;
		

		int slide=0,width=0;
		//fill the window content
		//computing L value
		cache = CacheAcqua.INSTANCE.getCache();
		
		if(outerContent == null){  // shen : outerContent will always be null?
			outerContent = new HashSet<Binding>();
		
			outerContentL=new HashMap<Binding,Double>();
			Context ec= context.getContext();
			tnow=Long.parseLong(ec.getAsString(Symbol.create("acqua:tnow")));	
			width=Integer.parseInt(ec.getAsString(Symbol.create("acqua:width")));
			slide= Integer.parseInt(ec.getAsString(Symbol.create("acqua:slide")));
			while(input.hasNext()){
				Binding b = input.next();
				BindingBase b1 = (BindingBase) b;
				Long t = null;
				while(b1 instanceof TimestampedBindingHashMap || b1.getParent()!=null){
					if(b1 instanceof TimestampedBindingHashMap){
						t = ((TimestampedBindingHashMap)b1).getMaxTimestamp();
						//logger.debug("- min ts ->"+((TimestampedBindingHashMap)b1).getMinTimestamp());
						break;
					}
					b1=(BindingBase) b1.getParent();
				}
				double L=Math.abs(Math.ceil((double)(t+width*1000L-tnow) / (double)(slide*1000L)));//L is always negative because tnow is always larger than entrance timestamp;
				//logger.debug("L value of the mapping{}: ceil({}+ {} -{} / {} )={}",b, t ,width, tnow,slide,L);
				//System.out.println("L value of the mapping"+b+": ceil("+t+"+ "+(width*1000L)+" -"+tnow+" / "+(slide*1000L)+" )="+L);
				BindingHashMap keyWindowBinding=new BindingHashMap();
				Iterator<Var> keyVars = CacheAcqua.INSTANCE.getKeyVars().iterator();
				while(keyVars.hasNext()){
					Var tempKeyVar=keyVars.next();
					if(b.get(tempKeyVar)!=null)
						{
							keyWindowBinding.add(tempKeyVar,b.get(tempKeyVar));
						}
				}
				outerContent.add(b);
				
				//logger.debug("adding L to outerContentL {}",outerContentL);
				Double prevL = outerContentL.get(keyWindowBinding);
				if(prevL==null)
					outerContentL.put(keyWindowBinding,L);
				else
					outerContentL.put(keyWindowBinding,Math.min(L,prevL));  // shen : why put min?
			}
			outerContentIterator =  new QueryIterPlainWrapper(outerContent.iterator());
		} else {
			logger.error("ourter content is not null ");
		}
		
		//if(QueryExecUtils.updateBudget>outerContent.size()) return ;//if the update budget is larger than window size it doesn't make sense to continue the execution
		
		//filling bbt and changeRate
		//elected set formation requires access to cache elements and their bbt and window setting
		cacheBBT=CacheAcqua.INSTANCE.getCacheBBT();
	    changeRate = CacheAcqua.INSTANCE.getCacheChangeRate();	
		ArrayList<keySet> scoreHM=new ArrayList<keySet>(); 
		ArrayList<keySet> notExpired=new ArrayList<keySet>(); 
		Iterator<Binding> windowKeyIt = outerContentL.keySet().iterator();
		while(windowKeyIt.hasNext()){
			Binding key = windowKeyIt.next();
			//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+key.get(Var.alloc("S")).toString());
			//logger.debug("key {} ",key);
			//System.out.println("@@@@@@@@@@@"+key+" changerate: "+changeRate.size()+","+changeRate.get(key));
			long bbt=cacheBBT.get(key);
			long cr=changeRate.get(key);
			double times = Math.ceil((double)(tnow-bbt) / (double)cr);
			
				
			double v = Math.ceil((bbt+cr*times-tnow)/(slide*1000L));
			//logger.debug("V value of the mapping{}: (bbt {} + cr {} * times {} -tnow {}) / slide {} )= {}",key, bbt ,cr, times,tnow,slide,v);					
			//System.out.println("V value of the mapping"+key+": (bbt "+bbt+" + cr "+cr+" * times "+times+" -tnow "+tnow+") / slide "+(slide*1000L)+" )= "+v);
			double l = outerContentL.get(key);
			//System.out.println("bbt->"+bbt+"  nowtime->"+ tnow);
			if(bbt <= tnow){//if cached entry is already expired we compute the V value
				scoreHM.add(new keySet(key, l, v));
				//-------------test
				/*Op op = QC.substitute(opService, key) ;
				Iterator<Binding> cb= cache.get(key).iterator();
				//silent = opService.getSilent() ;			
				QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
				Set<Binding> valuesOfKey= new HashSet<Binding>();
				while(qIter.hasNext()){
					Binding tempvalue=qIter.next();
					Binding cvalue = cb.next();
					if(tempvalue.get(Var.alloc("O2")).equals(cvalue.get(Var.alloc("O2")))){
						System.out.println("bbt->"+bbt+" tnow->"+tnow);
						System.out.println("last update time in server ->"+CacheAcqua.INSTANCE.lastUpdateTimeInRemote.get(key));
						System.out.println("these 2 should not be equal, key="+key+"changerate->"+cr);
						System.out.println(tempvalue);
						System.out.println(cvalue);		
						throw new RuntimeException();
					}
				}	
				*/
				
				
				//-------------
				//System.out.println("Expired!!! times >>>>"+times+" V >>>>"+v+" L >>>> "+l);
			}else {
				//-------------test
				/*Op op = QC.substitute(opService, key) ;
				Iterator<Binding> cb= cache.get(key).iterator();
				//silent = opService.getSilent() ;			
				QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
				Set<Binding> valuesOfKey= new HashSet<Binding>();
				while(qIter.hasNext()){
					Binding tempvalue=qIter.next();
					valuesOfKey.add(tempvalue); 
					//System.out.println("these two should not be equal");
					Binding cvalue = cb.next();
					if(!tempvalue.get(Var.alloc("O2")).equals(cvalue.get(Var.alloc("O2")))){
						System.out.println("bbt->"+bbt+" tnow->"+tnow);
						System.out.println("these 2 should be equal, key="+key);
						System.out.println(tempvalue);
						System.out.println(cvalue);		
						throw new RuntimeException();
					}
				}	*/
				//-------------
				notExpired.add(new keySet(key, l, v));
			}
		}
		Collections.sort(scoreHM, new ValueComparator());
		int updateBudget = QueryExecUtils.updateBudget;//* outerContent.size()/100;//Math.min(width,outerContent.size());//outerContent.size();//width/4;//slide;//0;//costPerAccess; 
        Iterator< keySet> sit= scoreHM.iterator();
        Iterator< keySet> fit= notExpired.iterator();
        int burn=0;
        for(int y=0;y<updateBudget ;y++){
        	keySet tempB;
        	if(sit.hasNext()){
        		tempB=sit.next();
        	}else if(fit.hasNext()){
        		burn ++;
        		tempB=fit.next();
        	}else {burn++; break;}
        	electedList.add(tempB.keys);
        	//logger.debug(">>>>>>>>>>>>>>>>> elected {} ",tempB.keys);
        }
        burnRate.add(burn);
        totalTimeCons=System.currentTimeMillis()-start;
        //System.out.println("for the full wbm all arrays should have same size: "+scoreHM.size()+","+electedList.size()+","+outerContent.size()+","+outerContentL.size());        
	}
	
	protected QueryIterator getInput() {
		return outerContentIterator;
	};

	
	@Override
	protected QueryIterator nextStage(Binding outerBinding)
	{
		long start=System.currentTimeMillis();
		
		//outerbinding contains the values for the window variables
		Binding key = extractKey(outerBinding);
		

		if(!electedList.contains(key)){
			//logger.debug(".........................not elected {} retrived from cache ",key);
			Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
			if(tmp!=null){
			QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
			
			//((BindingHashMap)outerBinding)
			QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
			totalTimeNext+=(System.currentTimeMillis()-start);
			return qIter2 ;}
			else{
				System.out.println(key+" windows entry without matching entry in cache!!!");
				return null;

			}
		}
		else{
			//boolean silent;
			try {
				callCount ++;
				Op op = QC.substitute(opService, outerBinding) ;
				//silent = opService.getSilent() ;			
				QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
				Set<Binding> valuesOfKey= new HashSet<Binding>();
				while(qIter.hasNext()){
					valuesOfKey.add(qIter.next()); 
				}
				cache.put(key, valuesOfKey);
				//-----------------------
				long bbt=cacheBBT.get(key);
				long cr=changeRate.get(key);
				double times = Math.ceil((double)(tnow-bbt) / (double)cr);
				cacheBBT.put(key, ((tnow / cr)+1) * cr);
						//bbt+cr*(long)times);
				//----------------------
				qIter = new QueryIterPlainWrapper(valuesOfKey.iterator());
				QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
				totalTimeNext+=(System.currentTimeMillis()-start);
				return qIter2;
			} catch (Exception ex)
			{
				/*if ( silent )
				{
					Log.warn(this, "SERVICE <" + opService.getService().toString() + ">: " + ex.getMessage()) ;
					// Return the input
					return QueryIterSingleton.create(outerBinding, getExecContext()) ; 
				}*/
				throw ex ;
			}

		}
	}

	
	private HashMap<Binding, Set<Binding>> Elect() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
class keySet{
	Binding keys;
	double vValue;
	double lValue;
	double scoreValue;
	
	public keySet(Binding b,double l,double v){
		keys=b;
		vValue=v;
		lValue=l;
		scoreValue=Math.min(v,l);
	}		
}
class ValueComparator implements Comparator<keySet> {

    keySet base;
    public int compare(keySet a, keySet b) {
        if (a.scoreValue > b.scoreValue) {
            return -1;
        }if(a.scoreValue < b.scoreValue) return 1;
        if(a.vValue>b.vValue) return -1;
        if(a.vValue<b.vValue) return 1;
        return 0;
    }
}
