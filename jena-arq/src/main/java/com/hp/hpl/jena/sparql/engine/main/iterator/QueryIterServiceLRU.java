package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Comparator;

import org.apache.jena.atlas.lib.Cache;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;

public class QueryIterServiceLRU extends QueryIterRepeatApply{
	OpService opService;
	Cache<Binding, Set<Binding>> cache;
	HashMap<Binding, Long> lastUpdateTimeInCache;
	public static long totalTimeCons = 0;
	public static long totalTimeNext = 0;
	public static int callCount = 0;

	public QueryIterServiceLRU(QueryIterator input, OpService opService,
			ExecutionContext context) {
		super(input, context);
		// System.out.println(">>>>>>>>>stack Trace of QueryIterService");
		// Thread.dumpStack();
		// if(QueryExecUtils.updateBudget>QueryExecUtils.windowLength) return ;
		callCount = 0;
		long start = System.currentTimeMillis();
		totalTimeCons = 0;
		totalTimeNext = 0;
		this.opService = opService;
		cache = CacheAcqua.INSTANCE.getCache();
		totalTimeCons = System.currentTimeMillis() - start;
		lastUpdateTimeInCache=CacheAcqua.INSTANCE.getLastUpdateTimeInCache();	    
	}

	
	@Override
	protected QueryIterator nextStage(Binding outerBinding) {
		final class BindingEntry{
			Binding userId;
			long updateTimeDiff;
			public BindingEntry(Binding id,long t){userId=id;updateTimeDiff=t;}
			public Binding getBinding(){return userId;}
		}
		long start=System.currentTimeMillis();
		//updating cache based on LRU
    	Random r=new Random();
    	List<BindingEntry> bindingUpdateLatency=new ArrayList<BindingEntry>();
    	Iterator<Binding> bIt= lastUpdateTimeInCache.keySet().iterator();
    	while(bIt.hasNext()){
    		Binding bId=bIt.next();
			long latestUpdateTime = Long.parseLong(lastUpdateTimeInCache.get(bId).toString());
			//long latestUpdateTime = Long.parseLong(bkgLastChangeTime.get(userid).toString());
			bindingUpdateLatency.add(new BindingEntry(bId, start-latestUpdateTime));				
		}
    	//we should pick the binding with the highest latency=> sort in decreasing order and pick the top element
		Collections.sort(bindingUpdateLatency, new Comparator<BindingEntry>() {

			public int compare(BindingEntry o1, BindingEntry o2) {
				int res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				//if(res==0)
				//	res=(int)(o2.updateTimeDiff - o1.updateTimeDiff);
				return res;
			}
		});
    	if (!(callCount >= QueryExecUtils.updateBudget))
		{
    		//System.err.println("budget left yet fetching from fuseki");
			Binding tmp = bindingUpdateLatency.iterator().next().getBinding();
			callCount ++;
			Op op = QC.substitute(opService, tmp) ;
			//silent = opService.getSilent() ;			
			QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
			Set<Binding> valuesOfKey= new HashSet<Binding>();
			while(qIter.hasNext()){
				valuesOfKey.add(qIter.next()); 
			}
			cache.put(tmp, valuesOfKey);	
			lastUpdateTimeInCache.put(tmp, start);
		}     	
    	
    	Binding key = extractKey(outerBinding);
    	Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
		if(tmp!=null){
		QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
		
		//((BindingHashMap)outerBinding)
		QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
		totalTimeNext+=(System.currentTimeMillis()-start);
		return qIter2 ;}
		else{
			System.out.println(key+" windows entry without matching entry in cache!!!");
			return null;

		}    	
	}

	

}
