/*
 * (c) Copyright 2007, 2008, 2009 Hewlett-Packard Development Company, LP
 * (c) Copyright 2010 Talis Systems Ltd
 * All rights reserved.
 * [See end of file]
 */

package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.apache.jena.atlas.lib.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.sparql.algebra.Op ;
import com.hp.hpl.jena.sparql.algebra.op.OpService ;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding ;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply ;
import com.hp.hpl.jena.sparql.engine.main.QC ;
import com.hp.hpl.jena.sparql.util.QueryExecUtils;


public class QueryIterServiceGlobal extends QueryIterRepeatApply
{
    OpService opService ;
    public static Logger logger = LoggerFactory.getLogger(QueryIterService.class);
	public static int callCount=0;
	public static long totalTimeNext=0;
	public static long totalTimeCons=0;
	private Cache<Binding,Set<Binding>> cache;
	
    public QueryIterServiceGlobal(QueryIterator input, OpService opService, ExecutionContext context)
    {
        super(input, context) ;
       ///System.out.println(">>>>>>>>>stack Trace of QueryIterService");
		//Thread.dumpStack();		
        callCount=0;
        totalTimeNext=0;
		totalTimeCons=0;
		long start=System.currentTimeMillis();
		this.opService = opService ;        
		totalTimeCons=System.currentTimeMillis()-start;
		cache = CacheAcqua.INSTANCE.getCache();
    }
    
    @Override
    protected QueryIterator nextStage(Binding outerBinding)
    {
    	//updating cache randomly
    	long start=System.currentTimeMillis();
		
    	Random r=new Random();
    	
    	if (!(callCount >= QueryExecUtils.updateBudget))
		{
    		//System.err.println("budget left yet fetching from fuseki");
			Map.Entry<Binding,Set<Binding>> tmp = (Entry<Binding, Set<Binding>>) cache.getEntrySet().toArray()[r.nextInt(cache.getEntrySet().size())];
			callCount ++;
			Op op = QC.substitute(opService, tmp.getKey()) ;
			//silent = opService.getSilent() ;			
			QueryIterator qIter = Service.exec((OpService)op, getExecContext().getContext()) ;
			Set<Binding> valuesOfKey= new HashSet<Binding>();
			while(qIter.hasNext()){
				valuesOfKey.add(qIter.next()); 
			}
			cache.put(tmp.getKey(), valuesOfKey);	
		}     	
    	
    	Binding key = extractKey(outerBinding);
    	Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
		if(tmp!=null){
		QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
		
		//((BindingHashMap)outerBinding)
		QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
		totalTimeNext+=(System.currentTimeMillis()-start);
		return qIter2 ;}
		else{
			System.out.println(key+" windows entry without matching entry in cache!!!");
			return null;

		}    	
    }
    
}
 

/*
 * (c) Copyright 2007, 2008, 2009 Hewlett-Packard Development Company, LP
 * (c) Copyright 2010 Talis Systems Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */