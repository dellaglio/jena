package com.hp.hpl.jena.rdf.model.impl;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class TimestampedStatementImpl extends StatementImpl {
	protected long timestamp;
	
	public TimestampedStatementImpl(Resource subject, Property predicate, RDFNode object, long timestamp, ModelCom model)	{
		super(subject, predicate, object, model);
	}

	public TimestampedStatementImpl(Resource subject, Property predicate, RDFNode object, long timestamp){
		super( subject, predicate, object );
	}  
	
	public long getTimestamp(){
		return timestamp;
	}
}
