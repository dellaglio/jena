1. clone it
2. import the projects in eclipse (one is ARQ, another is some test code)
3. if you cannot import jena-arq because it is already imported in eclipse, remove first all the apache-jena projects
4. modify the csparql pom: replace the version of jena-arq - it was 2.13.0, now it should be 2.13.1-SNAPSHOT