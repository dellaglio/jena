import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jena.atlas.io.IndentedWriter;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.core.DatasetGraphFactory;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.Plan;
import com.hp.hpl.jena.sparql.engine.QueryEngineFactory;
import com.hp.hpl.jena.sparql.engine.QueryEngineRegistry;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.ResultSetStream;
import com.hp.hpl.jena.sparql.engine.binding.BindingRoot;
import com.hp.hpl.jena.sparql.sse.WriterSSE;
import com.hp.hpl.jena.tdb.TDBFactory;




public class twitterClient {
public static void main(String[] args){
	SlidingWindow sw = new SlidingWindowFromStreamGeneratorFile(Config.INSTANCE.getQueryWindowWidth(), Config.INSTANCE.getQueryWindowSlide(), Config.INSTANCE.getQueryStartingTime(), Config.INSTANCE.getProjectPath() + Config.INSTANCE.getStreamFilePath());
	String queryString="select ?user " +
			"where { " +
			"?user <http://example.org/property/mentioned> ?mnumber . " +
				"SERVICE <http://localhost:3030/test/sparql> {" +
				"?user <http://example.org/property/followerCount> ?fnumber. "  +
				"?user <http://example.org/property/followerCount3> ?fnumber3. "  +
				"}" +
			"}" ;

	IndentedWriter out = new IndentedWriter(System.out, false);
    
    ResultSet resultSet;
	Query query = QueryFactory.create(queryString);
	Op op = Algebra.compile(query) ;
    Op op1 = op;
    VarExtractor serviceExt=new VarExtractor(OpService.class);
    VarExtractor allExt=new VarExtractor(null);
    WriterSSE.out(out, op1, query) ;
    System.out.println("############################################");
	System.out.println(serviceExt.extractTypeVars(op1));
	System.out.println(allExt.extractTypeVars(op1));
	System.out.println("############################################");
	
	op1 = Algebra.optimize(op);
    WriterSSE.out(out, op1, query) ;
	
	System.out.println("------------------------");
	
	while (sw.hasNext()) {
		Window w = sw.next();
		Map<Long, Long> entries = w.getDistinctEntriesAsMap();
		
		String directory = "tdb";
		Dataset dataset = TDBFactory.createDataset(directory);

		// assume we want the default model, or we could get a named model here
		Model tdb = dataset.getDefaultModel();
		tdb = ModelFactory.createMemModelMaker().createDefaultModel();
		Iterator<Long> users= entries.keySet().iterator();
		while (users.hasNext()){
			Long userid=users.next();
			System.out.println("adding "+userid +" into model of current window ");
		tdb.add(
				tdb.createResource("http://example.org/resource/user"+userid),
				tdb.createProperty("http://example.org/property/mentioned"),
				tdb.createLiteral(entries.get(userid).toString()));
		}	
		System.out.println("end of adding current window into model -------------------");

		DatasetGraph ds = DatasetGraphFactory.createOneGraph(tdb.getGraph());
        QueryEngineFactory f = QueryEngineRegistry.findFactory(op1, ds, null);
        System.out.println("type of queryenginefactory: "+f.getClass());
        Plan plan = f.create(op, ds, BindingRoot.create(), null) ;
        System.out.println("plan is : "+plan.toString());
        QueryIterator qIter = plan.iterator() ;

        System.out.println("query iter type >>> "+qIter.getClass());
        //System.out.println("variables >>> "+plan.);
        
        
		resultSet = new ResultSetStream(query.getResultVars(), tdb, qIter) ;
		ResultSetFormatter.out(System.out, resultSet);
		// System.out.println(entries.size());
		//join.process(w.getEndingTime(), w.getFrequencyOfEntitiesAsMap(), entries);// TwitterFollowerCollector.getInitialUserFollowersFromDB());//
	}
}

private static List<Var> extractAllQueryVars(Op op) {
	// TODO Auto-generated method stub
	return null;
}


}
