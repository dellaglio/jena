import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;


public class fusekiUpdater {
	public static Logger logger = LoggerFactory.getLogger(fusekiUpdater.class);
public static void main(String args[]){
	try{
		int maxChangeSec=500;
		HashMap<String, Integer> changeRate = new HashMap<String, Integer>();
		Random r = new Random();
		BufferedReader br = new BufferedReader(new FileReader(new File("C:/Users/sohdeh/Documents/GitHub/jena/acqua-client/data/userfollower100.ttl")));
		String line=null;
		while((line=br.readLine())!=null){
			String[] strs = line.split(" ");
			String userid = strs[0].substring(1,strs[0].length()-1);
			//System.out.println(userid);
			changeRate.put(userid, r.nextInt(maxChangeSec)+100);
		}
		
		
		
		 
		 
		while (true){
			Iterator<String> users = changeRate.keySet().iterator();
			while(users.hasNext()){
				long time=System.currentTimeMillis();
				String curUser = users.next();
				int cr=changeRate.get(curUser);
				int t= (int)time/1000;
				//System.out.println(curUser+" cr= "+cr+" t/m "+t);
				if (t % cr ==0)
				{
					logger.debug("update request user "+curUser);
					String UQ= 
							"DELETE    { <"+curUser+"> <http://example.org/follower> ?a } where{<"+curUser+"> <http://example.org/follower> ?a}; INSERT data {<"+curUser+"> <http://example.org/follower> <http://localhost/"+r.nextInt()+">} ";
					//System.out.println(UQ); 
					UpdateRequest query = UpdateFactory.create(UQ);					 
					 UpdateProcessor qexec=UpdateExecutionFactory.createRemoteForm(query,"http://localhost:3030/test/update");
					 qexec.execute();
				}
			}
			Thread.sleep(1000);
		}
	}catch(Exception e){e.printStackTrace();}
}
}
