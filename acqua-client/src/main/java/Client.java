

import org.apache.jena.atlas.io.IndentedWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.algebra.Algebra;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.core.DatasetGraphFactory;
import com.hp.hpl.jena.sparql.engine.Plan;
import com.hp.hpl.jena.sparql.engine.QueryEngineFactory;
import com.hp.hpl.jena.sparql.engine.QueryEngineRegistry;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.ResultSetStream;
import com.hp.hpl.jena.sparql.engine.binding.BindingRoot;
import com.hp.hpl.jena.sparql.sse.WriterSSE;
import com.hp.hpl.jena.tdb.TDBFactory;

public class Client {
	private final static String queryString = 
			"PREFIX xsd:  <http://www.w3.org/2001/XMLSchema#> " +
			"select * " +
			"where { " +
				"?city <http://dbpedia.org/property/name> ?name . " +
				"?city <http://dbpedia.org/property/state> ?state . " +
				"?city <http://purl.org/dc/terms/subject> ?topic . " +
				"SERVICE <http://dbpedia.org/sparql> {" +
					"?city <http://dbpedia.org/ontology/populationTotal> ?popCity. " +
					"?state <http://dbpedia.org/ontology/populationTotal> ?popstate. " +
				"}" +
			"}" +
			"LIMIT 10";

	private final static Logger logger = LoggerFactory.getLogger(Client.class);

	
	public static void main(String[] args) {
		logger.info("\n####################### Open the model #######################\n");
		String directory = "tdb";
		Dataset dataset = TDBFactory.createDataset(directory);

		// assume we want the default model, or we could get a named model here
		Model tdb = dataset.getDefaultModel();
		tdb = ModelFactory.createMemModelMaker().createDefaultModel();
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Milan"),
				tdb.createProperty("http://dbpedia.org/property/name"),
				tdb.createLiteral("Milano"));
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Milan"),
				tdb.createProperty("http://purl.org/dc/terms/subject"),
				tdb.createLiteral("http://dbpedia.org/resource/Category:Milan"));
		
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Milan"),
				tdb.createProperty("http://dbpedia.org/property/state"),
				tdb.createResource("http://dbpedia.org/resource/Italy"));
		
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Milan"),
				tdb.createProperty("http://dbpedia.org/property/state"),
				tdb.createResource("http://example.org/Europe"));
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Rome"),
				tdb.createProperty("http://dbpedia.org/property/name"),
				tdb.createLiteral("Rome"));
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Rome"),
				tdb.createProperty("http://purl.org/dc/terms/subject"),
				tdb.createLiteral("http://dbpedia.org/resource/Category:Capitals_in_Europe"));
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Rome"),
				tdb.createProperty("http://purl.org/dc/terms/subject"),
				tdb.createLiteral("http://dbpedia.org/resource/Category:Holy_cities"));
		tdb.add(
				tdb.createResource("http://dbpedia.org/resource/Rome"),
				tdb.createProperty("http://dbpedia.org/property/state"),
				tdb.createResource("http://dbpedia.org/resource/Italy"));

        IndentedWriter out = new IndentedWriter(System.out, false);
        
        ResultSet resultSet;
		Query query = QueryFactory.create(queryString);
		Op op = Algebra.compile(query) ;
        Op op1 = op;
        WriterSSE.out(out, op1, query) ;
		System.out.println("------------------------");
		op1 = Algebra.optimize(op);
        WriterSSE.out(out, op1, query) ;
		
		System.out.println("------------------------");

		DatasetGraph ds = DatasetGraphFactory.createOneGraph(tdb.getGraph());
        QueryEngineFactory f = QueryEngineRegistry.findFactory(op1, ds, null) ;
        Plan plan = f.create(op, ds, BindingRoot.create(), null) ;
        System.out.println(plan.toString());
        QueryIterator qIter = plan.iterator() ;

        System.out.println("query Vars >>> "+query.getResultVars());
        
		resultSet = new ResultSetStream(query.getResultVars(), tdb, qIter) ;
		ResultSetFormatter.out(System.out, resultSet);
	
	}
	
}
