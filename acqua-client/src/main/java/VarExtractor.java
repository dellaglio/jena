import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.algebra.Op;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpJoin;
import com.hp.hpl.jena.sparql.algebra.op.OpProject;
import com.hp.hpl.jena.sparql.algebra.op.OpService;
import com.hp.hpl.jena.sparql.core.Var;

public class VarExtractor {
private boolean flag=false;
private Class<?> extractorType=null;
VarExtractor(Class<?> op){this.extractorType=op; if(extractorType==null) flag=true;}
public Set<Var> extractTypeVars(Op op) {
	Set<Var> res=new HashSet<Var>();
	 //List<Triple> trs = ((OpBGP)((OpService)opj.getRight()).getSubOp()).getPattern().getList();
	if (op instanceof OpService)
	{
		if(extractorType==op.getClass() ) flag=true;
		return extractTypeVars(((OpService)op).getSubOp());
	}
    if (op instanceof OpBGP && flag){
    	 Iterator<Triple> trs = ((OpBGP)(op)).getPattern().getList().iterator();
    	 while (trs.hasNext()){
    		 Triple temp = trs.next();
    		 Node x=temp.getSubject();
    		 if(x instanceof Var ) res.add((Var)x);
    		 x=temp.getPredicate();
    		 if(x instanceof Var ) res.add((Var)x);
    		 x=temp.getObject();
    		 if(x instanceof Var ) res.add((Var)x);
    	 }
    	 return res;
    }
    if(op instanceof OpJoin)
    {
    	Set<Var> leftSerVars= extractTypeVars(((OpJoin)op).getLeft());
    	Set<Var> rightSerVars= extractTypeVars(((OpJoin)op).getRight());
    	Set<Var> total=new HashSet<Var>();
    	if(leftSerVars!=null) total.addAll(leftSerVars);
    	if(rightSerVars!=null) total.addAll(rightSerVars);
    	return total;
    }    
    if(op instanceof OpProject){
    	if(extractorType==op.getClass()) flag=true;
    	return extractTypeVars(((OpProject)op).getSubOp());
    }
    return null;
}
}
