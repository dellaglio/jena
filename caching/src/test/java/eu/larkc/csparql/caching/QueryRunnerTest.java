package eu.larkc.csparql.caching;

import java.util.HashSet;
import java.util.Set;

import org.apache.jena.atlas.lib.cache.CacheLRU;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.QueryIterator;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import com.hp.hpl.jena.sparql.engine.main.QC;
import com.hp.hpl.jena.util.cache.Cache;
import com.hp.hpl.jena.vocabulary.RDF;

import static org.junit.Assert.*;

public class QueryRunnerTest {
	@BeforeClass public static void init(){
		QC.setFactory(ARQ.getContext(), new OpExecutorFactoryAcqua());
	}
	
	private Model model;
	private String query;
	
	@Before public void setup(){
		model = ModelFactory.createDefaultModel();
		model.add(model.createResource("http://dbpedia.org/resource/Star_Wars_(film)"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://dbpedia.org/resource/The_Empire_Strikes_Back"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://es.dbpedia.org/resource/Star_Wars:_Episode_VI_-_Return_of_the_Jedi"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://dbpedia.org/resource/Star_Wars_Episode_I:_The_Phantom_Menace"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://dbpedia.org/resource/Star_Wars_Episode_II:_Attack_of_the_Clones"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://dbpedia.org/resource/Star_Wars_Episode_III:_Revenge_of_the_Sith"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
		model.add(model.createResource("http://dbpedia.org/resource/Star_Wars:_The_Force_Awakens"),
				RDF.type, 
				model.createResource("http://example.org/StarWarsMovie"));
	
		query = 
				"SELECT ?movie "
				+ "WHERE { "
				+ "?movie a <http://example.org/StarWarsMovie> "
				+ "SERVICE <http://dbpedia.org/sparql> {"
				+ "?movie a <http://dbpedia.org/ontology/Film> . "
				+ "?movie <http://dbpedia.org/ontology/director> <http://dbpedia.org/resource/George_Lucas> ."
				+ "?movie <http://dbpedia.org/ontology/director> ?actor "
				+ "} "
				/*+ "SERVICE <http://www.linkedmdb.org/snorql/> {"
				+ "?movie a <http://data.linkedmdb.org/resource/movie/film> . "
				+ "} "
				*/+ "} "
//				+ "LIMIT 20"
				;

	}
	
	@Test public void shouldRunQuery(){
		QueryRunner qr = new QueryRunner(query, model);
		
		Set<Node> expectedResult = new HashSet<Node>();
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_(film)"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_I:_The_Phantom_Menace"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_II:_Attack_of_the_Clones"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_III:_Revenge_of_the_Sith"));
		
		
		QueryIterator it = qr.execute();
		
		Set<Node> actualResult = new HashSet<Node>();
		while(it.hasNext()){
			Binding b = it.nextBinding();
			actualResult.add(b.get(Var.alloc("movie")));
		}
		
		assertEquals(expectedResult, actualResult);
	}
	
	@Test public void shouldFindSharedVariables(){
		QueryRunner qr = new QueryRunner(query, model);
		
		Set<Var> expectedResult = new HashSet<Var>();
		expectedResult.add(Var.alloc("movie"));
		assertEquals(expectedResult, qr.computeCacheKeyVars());
	}

	@Test public void shouldFindNonSharedServiceVariables(){
		QueryRunner qr = new QueryRunner(query, model);
		
		Set<Var> expectedResult = new HashSet<Var>();
		expectedResult.add(Var.alloc("actor"));
		assertEquals(expectedResult, qr.computeCacheValueVars());
	}
	
	@Test public void countServiceClauses(){
		QueryRunner qr = new QueryRunner(query, model);
		
		assertEquals(1, qr.countServiceClauses());
	}
	
	@Test public void shouldPassAllTheTests(){
		QueryRunner qr = new QueryRunner(query, model);
		
		assertEquals(1, qr.countServiceClauses());

		Set<Var> expectedSharedVariables = new HashSet<Var>();
		expectedSharedVariables.add(Var.alloc("movie"));
		
		assertEquals(expectedSharedVariables, qr.computeCacheKeyVars());

		Set<Var> expectedNonSharedVariables = new HashSet<Var>();
		expectedNonSharedVariables.add(Var.alloc("actor"));
		assertEquals(expectedNonSharedVariables, qr.computeCacheValueVars());

		Set<Node> expectedResult = new HashSet<Node>();
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_(film)"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_I:_The_Phantom_Menace"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_II:_Attack_of_the_Clones"));
		expectedResult.add(NodeFactory.createURI("http://dbpedia.org/resource/Star_Wars_Episode_III:_Revenge_of_the_Sith"));
		
		
		QueryIterator it = qr.execute();
		
		Set<Node> actualResult = new HashSet<Node>();
		while(it.hasNext()){
			Binding b = it.nextBinding();
			actualResult.add(b.get(Var.alloc("movie")));
		}
		
		assertEquals(expectedResult, actualResult);
	}
}
