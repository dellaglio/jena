package com.hp.hpl.jena.sparql.engine.main ;

import com.hp.hpl.jena.sparql.algebra.op.* ;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.main.iterator.QueryIterServiceWBM;

public class OpExecutorAcqua extends OpExecutor {
	protected OpExecutorAcqua(ExecutionContext execCxt){
		super(execCxt);
	}

	protected QueryIterator execute(OpService opService, QueryIterator input) {
//      return new QueryIterService(input, opService, execCxt) ;
		return new QueryIterServiceWBM(input, opService, execCxt) ;
//    	return new QueryIterServiceCache(input, opService, execCxt) ;    	
	}

}
