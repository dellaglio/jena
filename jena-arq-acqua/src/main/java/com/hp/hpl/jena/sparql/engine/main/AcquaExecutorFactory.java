package com.hp.hpl.jena.sparql.engine.main;

import com.hp.hpl.jena.sparql.engine.ExecutionContext;

//QC.setFactory(ARQ.getCOntext(), new AcquaExecutorFactory()) ;

public class AcquaExecutorFactory implements OpExecutorFactory {

	public OpExecutor create(ExecutionContext execCxt) {
		return new OpExecutorAcqua(execCxt);
	}

}
