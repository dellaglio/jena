/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hp.hpl.jena.sparql.engine.main.iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.plaf.SliderUI;

import org.apache.jena.atlas.lib.Cache;
import org.apache.jena.atlas.lib.cache.CacheLRU;
import org.apache.jena.atlas.logging.Log ;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.QueryExecException ;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.sparql.algebra.Op ;
import com.hp.hpl.jena.sparql.algebra.op.OpBGP;
import com.hp.hpl.jena.sparql.algebra.op.OpService ;
import com.hp.hpl.jena.sparql.core.Quad;
import com.hp.hpl.jena.sparql.core.Var;
import com.hp.hpl.jena.sparql.engine.ExecutionContext ;
import com.hp.hpl.jena.sparql.engine.QueryIterator ;
import com.hp.hpl.jena.sparql.engine.acqua.CacheAcqua;
import com.hp.hpl.jena.sparql.engine.binding.Binding ;
import com.hp.hpl.jena.sparql.engine.binding.BindingBase;
import com.hp.hpl.jena.sparql.engine.binding.BindingHashMap;
import com.hp.hpl.jena.sparql.engine.binding.TimestampedBindingHashMap;
import com.hp.hpl.jena.sparql.engine.http.Service ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIter ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterBlockTriples;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterCommonParent ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterPlainWrapper;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterRepeatApply ;
import com.hp.hpl.jena.sparql.engine.iterator.QueryIterSingleton ;
import com.hp.hpl.jena.sparql.engine.main.QC ;
import com.hp.hpl.jena.sparql.util.Context;
import com.hp.hpl.jena.sparql.util.Symbol;

public class QueryIterServiceCache extends QueryIterRepeatApply
{
	OpService opService ;
	//Set<Var> keyVars;
	Cache<Binding,Set<Binding>> cache;
	

	Set<Binding> electedList = new HashSet();
	public static Logger logger = LoggerFactory.getLogger(QueryIterServiceCache.class);
	
	public QueryIterServiceCache(QueryIterator input, OpService opService, ExecutionContext context)
	{
		super(input, context) ;
		if ( context.getContext().isFalse(Service.serviceAllowed) )
			throw new QueryExecException("SERVICE not allowed") ; 
		this.opService = opService ;
		
		cache = CacheAcqua.INSTANCE.getCache();		
	}
	
	
	@Override
	protected QueryIterator nextStage(Binding outerBinding)
	{
		
		//outerbinding contains the values for the window variables
		Binding key = extractKey(outerBinding);
		
		//System.out.println("required key : "+key+" cache content is ");
		//Iterator<Binding> cit = cache.keys();
		//while (cit.hasNext())
		//System.out.println(cit.next());
		//we should always retrive from cache but rather to check if we have update budget to fetch the data from service
		//we assume cache contains all the keys ==> materialize at the query registeration time
		//we fetch the response for a subset of bindings ...
		
		if(cache.containsKey(key)){
			Set<Binding> tmp = cache.get(key);//cache.get(outerBinding);
			
			QueryIterator qIter = new QueryIterPlainWrapper(tmp.iterator());
			
			//((BindingHashMap)outerBinding)
			QueryIterator qIter2 = new QueryIterCommonParent(qIter, outerBinding, getExecContext()) ;
			return qIter2 ;
		}
		else{
			System.out.println("windows entry without matching entry in cache!!!");
			return null;

		}
	}

	
	
//extract key of the current binding
	private Binding extractKey(Binding outerBinding) {
		BindingHashMap ret = new BindingHashMap();
		Set<Var> keyVars = CacheAcqua.INSTANCE.getKeyVars();
		for(Var v : keyVars){
			Node n = outerBinding.get(v);
			ret.add(v, n);
		}
		return ret;
	}
	
}
